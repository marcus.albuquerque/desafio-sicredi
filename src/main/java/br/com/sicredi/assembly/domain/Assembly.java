package br.com.sicredi.assembly.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Assembly {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    @Length(min = 3, max = 50)
    private String title;

    @Column(nullable = false)
    @Length(min = 5, max = 255)
    private String description;

    @Column(nullable = false)
    private Integer status;

    @Column(nullable = false)
    private LocalDateTime creationDate;
    private LocalDateTime updateDate;
    private LocalDateTime startDate;
    private LocalDateTime finishDate;
    private Integer votesResult;
    private Float percentage;

    @Column(nullable = false)
    private Long duration;

}
