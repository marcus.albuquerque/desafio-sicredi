package br.com.sicredi.assembly.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Associate {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable=false, unique=true)
    @Length(min = 11, max = 11)
    private String cpf;

    @Column(nullable = false)
    @Length(min = 3, max = 50)
    private String name;

    private Integer permission;

    @Column(nullable = false)
    private LocalDateTime creationDate;
    private LocalDateTime updateDate;

}

