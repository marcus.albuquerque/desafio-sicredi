package br.com.sicredi.assembly.controller;

import br.com.sicredi.assembly.controller.dto.VoteCreateDTO;
import br.com.sicredi.assembly.controller.dto.VoteReadDTO;
import br.com.sicredi.assembly.domain.Vote;
import br.com.sicredi.assembly.service.VoteService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping(value = "/api/vote")
public class VoteController {
	
	@Autowired
	private VoteService voteService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@PostMapping
	public ResponseEntity<VoteReadDTO> vote(
			@Valid @RequestBody VoteCreateDTO voteCreateDTO) {
		Vote vote = voteService.vote(voteCreateDTO);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(vote.getId()).toUri();
		return ResponseEntity.created(uri)
				.body(modelMapper.map(vote, VoteReadDTO.class));
	}

}
