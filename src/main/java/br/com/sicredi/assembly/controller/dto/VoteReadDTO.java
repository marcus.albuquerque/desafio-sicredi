package br.com.sicredi.assembly.controller.dto;

import br.com.sicredi.assembly.domain.enums.VoteChoice;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VoteReadDTO {
	
	private LocalDateTime creationDate;
	private String voteChoice;
	
	public void setVoteChoice(Integer voteChoice) {
		this.voteChoice = VoteChoice.toEnum(voteChoice).getDescription();
	}

}
