package br.com.sicredi.assembly.controller;

import br.com.sicredi.assembly.controller.dto.AssemblyCreateDTO;
import br.com.sicredi.assembly.controller.dto.AssemblyReadDTO;
import br.com.sicredi.assembly.controller.dto.AssemblyUpdateDTO;
import br.com.sicredi.assembly.domain.Assembly;
import br.com.sicredi.assembly.service.AssemblyService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/api/assembly")
public class AssemblyController {
	
	@Autowired
	private AssemblyService assemblyService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@PostMapping
	public ResponseEntity<AssemblyReadDTO> create(
			@Valid @RequestBody AssemblyCreateDTO assemblyCreateDTO) {
		Assembly assembly = assemblyService.create(assemblyCreateDTO);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(assembly.getId()).toUri();
		return ResponseEntity.created(uri)
				.body(modelMapper.map(assembly, AssemblyReadDTO.class));
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<AssemblyReadDTO> findById(@PathVariable Integer id) {
		Assembly assembly = assemblyService.findById(id);
		return ResponseEntity.ok(modelMapper.map(assembly, AssemblyReadDTO.class));
	}
	
	@GetMapping
	public ResponseEntity<List<AssemblyReadDTO>> list() {
		return ResponseEntity.ok(assemblyService.list());
	}
	
	@PutMapping
	public ResponseEntity<AssemblyReadDTO> update(
			@Valid @RequestBody AssemblyUpdateDTO assemblyUpdateDTO) {
		Assembly assembly = assemblyService.update(assemblyUpdateDTO);
		return ResponseEntity.ok(modelMapper.map(assembly, AssemblyReadDTO.class));
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		assemblyService.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	@PutMapping(value = "/startVoting/{id}")
	public ResponseEntity<AssemblyReadDTO> startVoting(@PathVariable Integer id) {
		Assembly assembly = assemblyService.startVoting(id);
		return ResponseEntity.ok(modelMapper.map(assembly, AssemblyReadDTO.class));
	}

}
