package br.com.sicredi.assembly.controller;

import br.com.sicredi.assembly.controller.dto.AssociateCreateDTO;
import br.com.sicredi.assembly.controller.dto.AssociateReadDTO;
import br.com.sicredi.assembly.controller.dto.AssociateUpdateDTO;
import br.com.sicredi.assembly.domain.Associate;
import br.com.sicredi.assembly.service.AssociateService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/api/associate")
public class AssociateController {
	
	@Autowired
	private AssociateService associateService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@PostMapping
	public ResponseEntity<AssociateReadDTO> create(
			@Valid @RequestBody AssociateCreateDTO associateCreateDTO) {
		Associate associate = associateService.create(associateCreateDTO);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(associate.getId()).toUri();
		return ResponseEntity.created(uri)
				.body(modelMapper.map(associate, AssociateReadDTO.class));
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<AssociateReadDTO> findById(@PathVariable Integer id) {
		Associate associate = associateService.findById(id);
		return ResponseEntity.ok(modelMapper.map(associate, AssociateReadDTO.class));
	}
	
	@GetMapping
	public ResponseEntity<List<AssociateReadDTO>> list() {
		return ResponseEntity.ok(associateService.list());
	}
	
	@PutMapping
	public ResponseEntity<AssociateReadDTO> update(
			@Valid @RequestBody AssociateUpdateDTO associateUpdateDTO) {
		Associate associate = associateService.update(associateUpdateDTO);
		return ResponseEntity.ok(modelMapper.map(associate, AssociateReadDTO.class));
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		associateService.delete(id);
		return ResponseEntity.noContent().build();
	}

}
